# ios-2c2p-sdk

[![CI Status](https://img.shields.io/travis/Pongsakorn Onsri/ios-2c2p-sdk.svg?style=flat)](https://travis-ci.org/Pongsakorn Onsri/ios-2c2p-sdk)
[![Version](https://img.shields.io/cocoapods/v/ios-2c2p-sdk.svg?style=flat)](https://cocoapods.org/pods/ios-2c2p-sdk)
[![License](https://img.shields.io/cocoapods/l/ios-2c2p-sdk.svg?style=flat)](https://cocoapods.org/pods/ios-2c2p-sdk)
[![Platform](https://img.shields.io/cocoapods/p/ios-2c2p-sdk.svg?style=flat)](https://cocoapods.org/pods/ios-2c2p-sdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ios-2c2p-sdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ios-2c2p-sdk'
```

## Author

Pongsakorn Onsri, pongsakorn@central.tech

## License

ios-2c2p-sdk is available under the MIT license. See the LICENSE file for more info.
