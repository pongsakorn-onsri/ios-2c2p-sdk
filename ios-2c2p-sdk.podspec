Pod::Spec.new do |s|
  s.name             = 'ios-2c2p-sdk'
  s.version          = '4.2.10'
  s.summary          = 'Unofficial supported for 2C2P iOS SDK'
  s.homepage         = 'https://bitbucket.org/pongsakorn-onsri/ios-2c2p-sdk'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pongsakorn Onsri' => 'pongsakorn@central.tech' }
  s.source           = {
      :http => "https://bitbucket.org/pongsakorn-onsri/ios-2c2p-sdk/raw/ff6ab7acb4138fc1fe7b0b4d67cf5cbae801f4a2/PGW.framework.zip"
  }
  s.ios.deployment_target = '10.0'
  s.static_framework        = true
  s.vendored_frameworks     = "PGW.framework"
  s.pod_target_xcconfig     = {
    "FRAMEWORK_SEARCH_PATHS" => "$(PODS_ROOT)/ios-2c2p-sdk/Frameworks",
    "LIBRARY_SEARCH_PATHS" => "$(TOOLCHAIN_DIR)/usr/lib/swift/$(PLATFORM_NAME)"
  }

end
